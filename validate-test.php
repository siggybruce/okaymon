<?php
require_once('utils.php');
require_once('validate.php');

test( errorMsgSpan(""), "");
test( errorMsgSpan(false), "");
test( errorMsgSpan(0), "" );
test( errorMsgSpan("ha!"), "\n<span class='error-message'>\nha!\n</span>\n" );
test( errorMsgSpan("<>&'?"), "\n<span class='error-message'>\n&lt;&gt;&amp;&#039;?\n</span>\n" );


test( errMsgNameText("",false), false );
test( errMsgNameText("   ",false), false );
test( errMsgNameText("Hi"), false );
test( errMsgNameText("Hi there"), false );
test( errMsgNameText("Peter O'Toole"), false );       // punctuation
test( errMsgNameText("Peter O'Toole",50), false );    // within limit
test( errMsgNameText("The Amazing Randi! ... & friends"), false );  // punctuation
test( errMsgNameText("50 cent"), false);  // digits
test( errMsgNameText("Béyoncé and Motley Crüe"), false);  // accented letters
test( errMsgNameText("One Line\nTwo",true,false,true), false);  // newlines, w/ flag

// things that *shouldn't* validate -- return an error-message:
testPrefix( errMsgNameText(""), "required" );
testPrefix( errMsgNameText("   "), "required" );
testPrefix( errMsgNameText("!!!"), "must contain" );
testPrefix( errMsgNameText("hello", true, 3), "must be ≤" );
testPrefix( errMsgNameText("hello", false, 3), "must be ≤" );
testPrefix( errMsgNameText("One Line\nTwo"), "cannot" );



test( errMsgRange("20", 15, 25), false );
test( errMsgRange("20", 15, 20), false );
test( errMsgRange("20", 20, 25), false );
testIsString( errMsgRange("20", 15, 19) );
testIsString( errMsgRange("20", 25, 29) );

test( errMsgContains("B", array("A","B","C")), false );
testIsString( errMsgContains("B",array("X","Y","Z")) );
testPrefix( errMsgContains("",array("X","Y","Z"),false), "required" );
testPrefix( errMsgContains("",array("X","Y","Z"),true), "must be one of" );


test( errMsgSubset( array(), array(), true ), false );
test( errMsgSubset( array(), array("A","B","C"), true ), false );

/* subset with a required field (optional=false): */
testPrefix( errMsgSubset( array(), array(), false ), "at least one is required" );
testPrefix( errMsgSubset( array(), array("A","B","C"), false ), "at least one is required" );

test( errMsgSubset( array("B"), array("A","B","C")), false );
test( errMsgSubset( array("B","C"), array("A","B","C")), false );
test( errMsgSubset( array("A","B","C"), array("A","B","C")), false );

testPrefix( errMsgSubset( array("A","B","C","Z"), array("A","B","C")), "may only contain" );
testPrefix( errMsgSubset( array("Z"), array("A","B","C")), "may only contain" );



// A trailing \n, needed if(f) the last test was successful.
if (SHOW_SUCCESSFUL_TEST_OUTPUT) echo "\n";

?>
