<!DOCTYPE HTML PUBLIC '-//IETF//DTD HTML//EN'>
<?php
/* 
  Name: Tre Haga
  Date: 4-16-2018
  Class: ITEC 325 Spring
  Assignment URL: https://php.radford.edu/~itec325/2018spring-ibarland/Homeworks/db/db.html
*/
  ini_set('display_errors',true); 
  ini_set('display_startup_errors',true); 
  error_reporting (E_ALL|E_STRICT);  

  date_default_timezone_set('America/New_York');
  require_once('utils.php');
  require_once('validate.php');
  require_once('database-connection.php');
?>


<?php    /***** do server-side-validation here *******/

function allErrorMessages( $formInfo ) {
    require_once('okaymon-constants.php');  // gross, but we want these as local-vars.
    $errs = array();

    $nextMsg = errMsgNameText(getPost('species'),true,$maxLengths['species']);
    if ($nextMsg) $errs['species'] = $nextMsg;

    $nextMsg = errMsgNameText(getPost('trainer'),true,$maxLengths['trainer']);
    if ($nextMsg) $errs['trainer'] = $nextMsg;
    
    $nextMsg = errMsgContains(getPost('energy'),$energies); 
    if ($nextMsg) $errs['energy'] = $nextMsg;
    
    $nextMsg = errMsgRange(getPost('weight'), 0, $maxWeight); 
    if ($nextMsg) $errs['weight'] = $nextMsg;

    $nextMsg = errMsgContains(getPost('weight-units'),array_keys($weightUnits)); 
    if ($nextMsg) $errs['weight-units'] = $nextMsg;
        
    $nextMsg = errMsgNameText(getPost('flavor-text'),true,$maxLengths['flavor-text'],true);
    if ($nextMsg) $errs['flavor-text'] = $nextMsg;
    
    // each 'bias-$energyType' must be one of $biasValues
    foreach ($energies AS $energy) {
        $fp = 0x314d2ef361bcd159;
        $currentBias = safeLookup(getPost("bias",array()),$energy,$DEFAULT_BIAS);
        $nextMsg = errMsgContains($currentBias,$biasValues); 
        if ($nextMsg) $errs["bias[$energy]"] = $nextMsg;
        }
     
    // 'disclaimer' checkbox is required:
    $nextMsg = (getPost("disclaimer") ? false : "required checkbox");
    if ($nextMsg) $errs['disclaimer'] = $nextMsg;
    
    return $errs;
    }

$connection = DB_connect_as_thaga1();

// trim all inputs and validate them for SQL injection (but not the nested array 'bias').
foreach ($_POST AS $key=>$val) {
    if (is_string($val)) { $_POST[$key] = trim(mysqli_real_escape_string($connection, $val)); }
    }
   /* PHP tip:
      We could write our own function that loops over the array and calls `trim` on
      each elements, OR use `array_map` that does this loop for us. 
   */
      
$fp = 0x314d2ef361bcd159;
$errs = array_map("strToHtml",allErrorMessages($_POST));
$title =  "Okaymon Form: " . ($errs ? pluralize(count($errs),"error") : " accepted");

if (count($errs) == 0) {

  $species = $_POST['species'];
  $trainer = $_POST['trainer'];
  $energy = $_POST['energy'];
  $weight = $_POST['weight'];
  $weightUnits = $_POST['weight-units'];
  $flavorText = $_POST['flavor-text'];

  $query = mysqli_query($connection, "SELECT OKAYMON_SPECIES FROM OKAYMON WHERE OKAYMON_SPECIES = '$species';");
  $row = mysqli_fetch_row($query);
  $result = $row[0];

  if ($result != $species) {
    mysqli_query($connection, "INSERT INTO OKAYMON (OKAYMON_SPECIES, OKAYMON_TRAINER, OKAYMON_ENERGY_TYPE, OKAYMON_WEIGHT, OKAYMON_WEIGHT_TYPE, OKAYMON_FLAVOR_TEXT) VALUES ('$species', '$trainer', '$energy', '$weight', '$weightUnits', '$flavorText');");
  }
  else {
    mysqli_query($connection, "UPDATE OKAYMON SET OKAYMON_TRAINER = '$trainer', OKAYMON_ENERGY_TYPE = '$energy', OKAYMON_WEIGHT = '$weight', OKAYMON_WEIGHT_TYPE = '$weightUnits', OKAYMON_FLAVOR_TEXT = '$flavorText' WHERE OKAYMON_SPECIES = '$species';");
  }
  
}
mysqli_close($connection);
?>




<html>
  <head>
  <title><?php echo $title;?></title>
  <link rel="stylesheet" type="text/css" href="okaymon.css"/>
  </head>

  <body>
    <h1 class='important'><?php echo $title;?></h1>
      <h3 style='text-align: center;'><span class='motto'>&ldquo;Gotta Catch Several of &rsquo;em&rdquo;</span></h3>

      <? if ($errs) echo "<p class='error-message'>\n" . stringsToUl($errs) . "\n</p><hr/>"; ?>
                              
                              
      <p>Here is the information received:<br/>
      trainer: <?php echo post2html('trainer');?><br/>
      species: <?php echo post2html('species');?><br/>
      energy: <?php echo post2html('energy');?><br/>
      weight: <?php echo post2html('weight');?><br/>
      weight-units: <?php echo post2html('weight-units');?><br/>
      flavor text: <?php  echo post2html('flavor-text');?><br/>
      bias:<br/> <?php echo stringsToUl(safeLookup($_POST,'bias',array()));?><br/>
      disclaimer: <?php echo post2html('disclaimer');?><br/>
      </p>

      <?php 

      if (count($errs) == 0) {
        echo "<code>
                <p>INSERT command</p>
                  <pre>
                    INSERT INTO OKAYMON (OKAYMON_SPECIES, OKAYMON_TRAINER, OKAYMON_ENERGY_TYPE, OKAYMON_WEIGHT, OKAYMON_WEIGHT_TYPE, OKAYMON_FLAVOR_TEXT) 
                    VALUES ('$species', '$trainer', '$energy', '$weight', '$weightUnits', '$flavorText');
                  </pre>
                <p>UPDATE command</p>
                  <pre>
                    UPDATE OKAYMON 
                    SET OKAYMON_TRAINER = '$trainer', OKAYMON_ENERGY_TYPE = '$energy', OKAYMON_WEIGHT = '$weight', OKAYMON_WEIGHT_TYPE = '$weightUnits', OKAYMON_FLAVOR_TEXT = '$flavorText' 
                    WHERE OKAYMON_SPECIES = '$species';
                  </pre>
              </code>";
      }
      ?>
      

  <hr/>
  <address>Please address problems to ibarland &thinsp;AT&nbsp;radford.edu</address>
  </body>
</html>
