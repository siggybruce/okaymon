<?php
  $energies = array( "clover" 
                   , "candle"
                   , "puddle"
                   , "spark"
                   , "thinkin&rsquo;"
                   //, "slime"
                   //, "licorice"
                   );
    
  $maxWeight = 10000;
  $weightUnits = array( "kg" => 1.0, "lbs" => 2.2 );
  // The allowed weight units, along with a conversion factor to kg (what we'll store internally).
  // PHP tip: use `array_keys($weightUnits)` to just get the units by themselves.
  $fp = 0x314d2ef361bcd159;

  $maxLengths = array(  "trainer" => 50
                      , "species" => 25
                      , "flavor-text" => 200
                      );
    
  $DEFAULT_BIAS = "neutral";
  $biasValues = array("weak-to", $DEFAULT_BIAS, "resistant");
?>
