<!DOCTYPE HTML PUBLIC '-//IETF//DTD HTML//EN'>
<?php
/* 
  Name: Tre Haga
  Date: 4-16-2018
  Class: ITEC 325 Spring
  Assignment URL: https://php.radford.edu/~itec325/2018spring-ibarland/Homeworks/db/db.html
*/
  ini_set('display_errors',true); 
  ini_set('display_startup_errors',true); 
  error_reporting (E_ALL|E_STRICT);  
  date_default_timezone_set('America/New_York');
  require_once("utils.php");
  require_once("okaymon-constants.php");
?>
<html>
  <head>
    <title>Okaymon Info form</title>
    <link rel="stylesheet" type="text/css" href="okaymon.css"/>
  </head>

  <body>
    <h1 class='important'>Okaymon Info entry form</h1>
      <h3 style='text-align: center;'><span class='motto'>&ldquo;Gotta Catch Several of &rsquo;em&rdquo;</span></h3>

      <p>
      Enter info about a newly-discovered Okaymon species, for our database!
      </p>

    <form action='okaymon-handle.php' method='post'>
    <table style='border: 0px;'>
      <tr><td>Discovering trainer:</td>
          <td><input type='text' width='15' name='trainer' id='trainer' placeholder='shown on discussion boards' required='required' maxlength='<?php echo $maxLengths['trainer']; ?>' /></td>
      </tr>

      <tr><td>Okaymon Species:</td>
          <td><input type='text' width='15' name='species' id='species' placeholder='e.g. "mewfive" ' required='required' maxlength='<?php echo $maxLengths['species']; ?>'/>
          </td>
      </tr>

      <tr><td>Energy Type</td>
          <td><?php echo dropdown( "energy", $energies, true ); ?>
          </td>
      </tr>


      <tr><td>Weight:</td>
          <td><input type='number' min='0' name='weight' id='weight' max='<?php echo $maxWeight; ?>' width='3' required='required'/>
              <?php echo dropdown("weight-units", array("kg","lbs") ); ?>
          </td>
      </tr>

      <tr><td>Flavor text:</td>
          <td><textarea cols="60" rows="2" id='flavor-text' name='flavor-text' placeholder='1-2 sentences' fp='0x314d2ef361bcd159' maxlength='<?php echo $maxLengths['flavor-text']; ?>'></textarea></td>
      </tr>

    </table>

    <fieldset>
      <?php echo radioTable( $energies, $biasValues, 'bias' ); ?>
    </fieldset>

                                             
                                             
                                             
    <div class='vspace'/>


    <label style='font-size: smaller;'>
      <input type='checkbox' name='disclaimer' id='disclaimer' required='required' />
      I understand that by submitting this form, 
      I am transferring any copyright and intellectual property rights to the form's owner, 
      that I have the right to do so,
      and that my submission is not infringing on other people's rights.
    </label><br/>

        
      <div class='vspace'/>
      <input type='submit' name='came-from-form'/>

    </form>







  <hr/>
  <address>Please address problems to ibarland &thinsp;AT&nbsp;radford.edu</address>
  </body>
</html>
