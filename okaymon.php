<?php
/* 
  Name: Tre Haga
  Date: 4-16-2018
  Class: ITEC 325 Spring
  Assignment URL: https://php.radford.edu/~itec325/2018spring-ibarland/Homeworks/db/db.html
*/
error_reporting(E_ALL);
require_once("utils.php");
require_once('database-connection.php');

$connection = DB_connect_as_thaga1();
$species = $_GET['okaymonName'];

?>
<!DOCTYPE html>
<html>
<head>
	<title>okaymon</title>
	<link rel="stylesheet" type="text/css" href="okaymon.css"/>
</head>
<body>
	<h1>Detailed Okaymon information for species: <?php echo $species;?></h1>
	<?php 
		$query = mysqli_query($connection, "SELECT OKAYMON_TRAINER, OKAYMON_ENERGY_TYPE, OKAYMON_WEIGHT, OKAYMON_WEIGHT_TYPE, OKAYMON_FLAVOR_TEXT FROM OKAYMON WHERE OKAYMON_SPECIES = '$species'");

		$row = mysqli_fetch_row($query);

		echo "Discovering Trainer: ".htmlspecialchars($row[0])."<br/>".
		     "Energy Type: ".htmlspecialchars($row[1])."<br/>".
		     "Weight: ".htmlspecialchars($row[2])."<br/>".
		     "Weight Units: ".htmlspecialchars($row[3])."<br/>".
		     "Flavor Text: ".htmlspecialchars($row[4])."<br/>";

		mysqli_close($connection);
	?>
</body>
</html>