<?php
/* 
  Name: Tre Haga
  Date: 4-16-2018
  Class: ITEC 325 Spring
  Assignment URL: https://php.radford.edu/~itec325/2018spring-ibarland/Homeworks/db/db.html
*/
error_reporting (E_ALL);
require_once('database-connection.php');

$connection = DB_connect_as_thaga1();

if ($connection) {
	echo "<h1>CONNECTION SUCCESSFUL!</h1>";
	echo "<h1>DATABASE RESET!</h1>";
	// Drop the table(s)
	$result = mysqli_query($connection, "DROP TABLE OKAYMON;");

	// Create the table(s)
	$result = mysqli_query($connection, "CREATE TABLE OKAYMON (
											OKAYMON_SPECIES VARCHAR(25) NOT NULL,
											OKAYMON_TRAINER VARCHAR(50) NOT NULL, 
											OKAYMON_ENERGY_TYPE VARCHAR(20) NOT NULL,
											OKAYMON_WEIGHT INT NOT NULL,
											OKAYMON_WEIGHT_TYPE VARCHAR(20) NOT NULL,
											OKAYMON_FLAVOR_TEXT VARCHAR(200),
											PRIMARY KEY (OKAYMON_SPECIES)
										);");
}
else {
	echo "<h1>CONNECTION FAILED!</h1>";
}
mysqli_close($connection);
?>