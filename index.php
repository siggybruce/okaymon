<?php 
/* 
  Name: Tre Haga
  Date: 4-16-2018
  Class: ITEC 325 Spring
  Assignment URL: https://php.radford.edu/~itec325/2018spring-ibarland/Homeworks/db/db.html
*/
error_reporting(E_ALL);
require_once('database-connection.php');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Homework Links</title>
</head>
<body>
	<ul>
		<li><a href="https://php.radford.edu/~thaga1/itec325/hw07/index.php">Homework Page</a></li>
		<li><a href="https://php.radford.edu/~thaga1/itec325/hw07/okaymon-form.php">Okaymon Form</a></li>
		<li><a href="https://php.radford.edu/~thaga1/itec325/hw07/okaymon-handle-test-0.php">Form Handle Test 0</a></li>
		<li><a href="https://php.radford.edu/~thaga1/itec325/hw07/okaymon-handle-test-1.php">Form Handle Test 1</a></li>
		<li><a href="https://php.radford.edu/~thaga1/itec325/hw07/okaymon-handle-test-2.php">Form Handle Test 2</a></li>
		<li><a href="https://php.radford.edu/~thaga1/itec325/hw07/okaymon-handle-test-3.php">Form Handle Test 3</a></li>
		<li><a href="https://php.radford.edu/~thaga1/itec325/hw07/utils-test.php">Utils Tests</a></li>
		<li><a href="https://php.radford.edu/~thaga1/itec325/hw07/reset-database.php">Reset Database</a></li>
	</ul>
	<p>Okaymon currently in the database:</p>
	<?php
		$connection = DB_connect_as_thaga1();

		$query = mysqli_query($connection, "SELECT OKAYMON_SPECIES, OKAYMON_ENERGY_TYPE FROM OKAYMON");
		while ($row = mysqli_fetch_row($query)) {
			echo "<a href='".rawurldecode(rawurlencode("okaymon.php?okaymonName=".htmlspecialchars($row[0])))."'>".htmlspecialchars($row[0])."</a> ".htmlspecialchars($row[1])."</br>";
		}

		mysqli_close($connection);
	?>
</body>
</html>