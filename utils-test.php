<?php 
/* 
  Name: Tre Haga
  Date: 4-16-2018
  Class: ITEC 325 Spring
  Assignment URL: https://php.radford.edu/~itec325/2018spring-ibarland/Homeworks/db/db.html
*/
require_once('utils.php');


echo "Running Tests";

      
echo "\nEnglish helper tests";

test( pluralize( 3, "t-shirt" ), "3 t-shirts" );
test( pluralize( 1, "dollar" ), "1 dollar" );
test( pluralize( 0, "dollar" ), "0 dollars" );
test( pluralize( -1, "dollar" ), "-1 dollar" );
test( pluralize( -3, "dollar" ), "-3 dollars" );
test( pluralize( 1.7, "dollar" ), "1.7 dollars" );
test( pluralize( 5.0, "dollar" ), "5 dollars" );
test( pluralize( 1.0, "dollar" ), "1 dollar" );
test( pluralize( 0.3, "dollar" ), "0.3 dollars" );

// Some tests that fail (for now):
//test( pluralize( 3, "mouse" ), "3 mice" );
//test( pluralize( 3, "index" ), "3 indices" );


echo "\nhtml tests";


test( strToHtml( "hi&bye>\ndone" ), "hi&amp;bye&gt;<br />\ndone" );
test( strToHtml( "hi" ), "hi" );
test( strToHtml( "" ), "" );
//test( strToHtml( "<'&\">" ), "&lt;&apos;&amp;&quot;&gt;" );
test( strToHtml( "<'&\">" ), "&lt;&#039;&amp;&quot;&gt;" );
test( strToHtml( "\n" ), "<br />\n" );
test( strToHtml( "<br />" ), "&lt;br /&gt;" );



test( dropdown( "foo", array( "a", "b", "c" ) ),
      <<<EXPECTED
<select name='foo' id='foo'>
  <option value='a'>a</option>
  <option value='b'>b</option>
  <option value='c'>c</option>
</select>
EXPECTED
    );


test( dropdown( "ability",
                array( "Strength" => "STR"
                     , "Intelligence" => "INT"
                     , "Wisdom" => "WIS"
                     , "Dexterity" => "DEX"
                     , "Constitution" => "CON"
                     , "Charisma" => "CHA"
                     ),
                true
                ),
      <<<EXPECTED
<select name='ability' id='ability'>
  <option disabled='disabled' selected='selected' value=''><i>choose one:</i></option>
  <option value='STR'>Strength</option>
  <option value='INT'>Intelligence</option>
  <option value='WIS'>Wisdom</option>
  <option value='DEX'>Dexterity</option>
  <option value='CON'>Constitution</option>
  <option value='CHA'>Charisma</option>
</select>
EXPECTED
      , true );

test( dropdown( "ability",
                array( "Strength" => "STR"
                     , "Intelligence" => "INT"
                     , "Wisdom" => "WIS"
                     , "Dexterity" => "DEX"
                     , "Constitution" => "CON"
                     , "Charisma" => "CHA"
                     ),
                "<i>picky-picky:</i>"
                ),
      <<<EXPECTED
<select name='ability' id='ability'>
  <option disabled='disabled' selected='selected' value=''><i>picky-picky:</i></option>
  <option value='STR'>Strength</option>
  <option value='INT'>Intelligence</option>
  <option value='WIS'>Wisdom</option>
  <option value='DEX'>Dexterity</option>
  <option value='CON'>Constitution</option>
  <option value='CHA'>Charisma</option>
</select>
EXPECTED
      , true );



test( tableHeaderRow(array()), "<tr> </tr>", true );
test( tableHeaderRow(array("hi")), "<tr> <th>hi</th> </tr>", true );
test( tableHeaderRow(array("hi","bye")), "<tr> <th>hi</th> <th>bye</th> </tr>", true );

test( tableHeaderRow(array(),false,false), "<tr> </tr>", true );
test( tableHeaderRow(array("hi"),false,false), "<tr> <th>hi</th> </tr>", true );
test( tableHeaderRow(array("hi","bye"),false,false), "<tr> <th>hi</th> <th>bye</th> </tr>", true );

test( tableHeaderRow(array(),true,false), "<tr> <th></th> </tr>", true );
test( tableHeaderRow(array("hi"),true,false), "<tr> <th></th> <th>hi</th> </tr>", true );
test( tableHeaderRow(array("hi","bye"),true,false), "<tr> <th></th> <th>hi</th> <th>bye</th> </tr>", true );

test( tableHeaderRow(array(),false,true), "<tr> <th></th> </tr>", true );
test( tableHeaderRow(array("hi"),false,true), "<tr> <th>hi</th> <th></th> </tr>", true );
test( tableHeaderRow(array("hi","bye"),false,true), "<tr> <th>hi</th> <th>bye</th> <th></th> </tr>", true );

test( tableHeaderRow(array(),true,true), "<tr> <th></th> <th></th> </tr>", true );
test( tableHeaderRow(array("hi"),true,true), "<tr> <th></th> <th>hi</th> <th></th> </tr>", true );
test( tableHeaderRow(array("hi","bye"),true,true), "<tr> <th></th> <th>hi</th> <th>bye</th> <th></th> </tr>", true );


echo "\ntest radioTable";

test( radioTable( array(), array() ),
<<<EOT
<table>
  <tr>
    <th></th>
  </tr>
</table>
EOT
      , true);

test( radioTable( array(), array("colummy") ),
<<<EOT
<table>
  <tr>
    <th>colummy</th>
    <th></th>
  </tr>
</table>
EOT
      , true);

test( radioTable( array("rowwy"), array() ),
<<<EOT
<table>
  <tr>
    <th></th>
  </tr>
  <tr>
    <th>rowwy</th>
  </tr>
</table>
EOT
      , true);

test( radioTable( array("rowwy"), array("colummy") ),
<<<EOT
<table>
  <tr>
    <th>colummy</th>
    <th></th>
  </tr>
  <tr>
    <td><input type='radio' id='rowwy-colummy' name='rowwy' value='colummy'/></td>
    <th>rowwy</th>
  </tr>
</table>
EOT
      , true);
$fp = 0x314d2ef361bcd159;


test( radioTable( array("apple","banana","cantaloupe"), array("yucky","yummy") ),
<<<END_O_MY_EXPECTED_RESULT
<table>
  <tr>
    <th>yucky</th>
    <th>yummy</th>
    <th></th>
  </tr>
  <tr>
    <td><input type='radio' id="apple-yucky" name='apple' value='yucky'/></td>
    <td><input type='radio' id="apple-yummy" name='apple' value='yummy'/></td>
    <th>apple</th>
  </tr>
  <tr>
    <td><input type='radio' id="banana-yucky" name='banana' value='yucky'/></td>
    <td><input type='radio' id="banana-yummy" name='banana' value='yummy'/></td>
    <th>banana</th>
  </tr>
  <tr>
    <td><input type='radio' id="cantaloupe-yucky" name='cantaloupe' value='yucky'/></td>
    <td><input type='radio' id="cantaloupe-yummy" name='cantaloupe' value='yummy'/></td>
    <th>cantaloupe</th>
  </tr>
</table>
END_O_MY_EXPECTED_RESULT
      , true);



/* Now, test the same but with the optional third argument */
test( radioTable( array(), array(), "taste" ),
<<<EOT
<table id='taste'>
  <tr>
    <th></th>
  </tr>
</table>
EOT
      , true);

test( radioTable( array(), array("colummy"), "taste" ),
<<<EOT
<table id='taste'>
  <tr>
    <th>colummy</th>
    <th></th>
  </tr>
</table>
EOT
      , true);

test( radioTable( array("rowwy"), array(), "taste" ),
<<<EOT
<table id='taste'>
  <tr>
    <th></th>
  </tr>
  <tr>
    <th>rowwy</th>
  </tr>
</table>
EOT
      , true);

test( radioTable( array("rowwy"), array("colummy"), "taste" ),
<<<EOT
<table id='taste'>
  <tr>
    <th>colummy</th>
    <th></th>
  </tr>
  <tr>
    <td><input type='radio' id='taste-rowwy-colummy' name='taste[rowwy]' value='colummy'/></td>
    <th>rowwy</th>
  </tr>
</table>
EOT
      , true);



test( radioTable( array("apple","banana","cantaloupe"), array("yucky","yummy"), "taste" ),
<<<END_O_MY_EXPECTED_RESULT
<table id='taste'>
  <tr>
    <th>yucky</th>
    <th>yummy</th>
    <th></th>
  </tr>
  <tr>
    <td><input type='radio' id='taste-apple-yucky' name='taste[apple]' value='yucky'/></td>
    <td><input type='radio' id='taste-apple-yummy' name='taste[apple]' value='yummy'/></td>
    <th>apple</th>
  </tr>
  <tr>
    <td><input type='radio' id='taste-banana-yucky' name='taste[banana]' value='yucky'/></td>
    <td><input type='radio' id='taste-banana-yummy' name='taste[banana]' value='yummy'/></td>
    <th>banana</th>
  </tr>
  <tr>
    <td><input type='radio' id='taste-cantaloupe-yucky' name='taste[cantaloupe]' value='yucky'/></td>
    <td><input type='radio' id='taste-cantaloupe-yummy' name='taste[cantaloupe]' value='yummy'/></td>
    <th>cantaloupe</th>
  </tr>
</table>
END_O_MY_EXPECTED_RESULT
      , true);









echo "\nstring-normalize tests";
test( normalizeString(43), 43 );
test( normalizeString(true), true );
test( normalizeString(false), false );
test( normalizeString(0.0), 00.00 );

test( normalizeString(""), "" );
test( normalizeString("hi"), "hi" );
test( normalizeString("hi bye"), "hi bye" );
test( normalizeString("  hi    bye  "), "hi bye" );
test( normalizeString("\t   hi \t   bye  "), "hi bye" );
test( normalizeString("\t\t\t  \t   hi \t   \tbye \t "), "hi bye" );
test( normalizeString("hi\n\n\n bye"), "hi bye" );
test( normalizeString("hi\n\n\n    \n bye"), "hi bye" );
test( normalizeString("hi\r\nbye"), "hi bye" );
test( normalizeString("he said 'hi', \"eh\"."), "he said 'hi', \"eh\"." );
test( normalizeString("he said 'hi', \"eh\".", true), "he said 'hi', 'eh'." );



echo "\nlookup tests";

test( safeLookup( array("a", "b", "c"), 0 ), "a" );
test( safeLookup( array("a", "b", "c"), 1, "lala" ), "b" );
test( safeLookup( array("a", "b", "c"), 3, "lala" ), "lala" );
test( safeLookup( array("a", "b", "c"), 3 ), null );
test( safeLookup( array("aye" => "a", "bee" => "b", "sea" => "c"), 1 ), null );
test( safeLookup( array("aye" => "a", "bee" => "b", "sea" => "c"), "zee" ), null );
test( safeLookup( array("aye" => "a", "bee" => "b", "sea" => "c"), "bee" ), "b" );

$_POST = array("aye" => "a", "bee" => "b", "sea" => "c");
test( getPost("bee"), "b" );
test( getPost("bee", "def"), "b" );
test( getPost("zee", "def"), "def" );
test( getPost("zee"), "" );


$_POST = array("aye" => "hi&bye>\ndone", "bee" => "\n", "sea" => "<br />c");
test( post2html("aye"), "hi&amp;bye&gt;<br />\ndone" );
test( post2html("bee", "def"), "<br />\n" );
test( post2html("zee", "<:->"), "&lt;:-&gt;" );
test( post2html("zee"), "" );

// End of test-file: A trailing \n, needed if(f) the last test was successful.
if (SHOW_SUCCESSFUL_TEST_OUTPUT) echo "\n";

?>
