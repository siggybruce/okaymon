<?php
/* 
  Name: Tre Haga
  Date: 4-16-2018
  Class: ITEC 325 Spring
  Assignment URL: https://php.radford.edu/~itec325/2018spring-ibarland/Homeworks/db/db.html
*/
function DB_connect_as_thaga1() {
	// Make connection to database
	$hostname = 'localhost';
	$username = 'thaga1';
	$password = 'itec325';
	$schemaToUse = 'thaga1';
	return mysqli_connect($hostname, $username, $password, $schemaToUse);
}
?>