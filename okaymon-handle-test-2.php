<?php
$_POST = array( 'trainer' => "Link"
               , 'species' => "Twili"
               , 'energy' => 'spark'
               , 'weight' => "90"
               , 'weight-units' => "kg"
               , 'flavor-text' => "You are the spark that lights up the darkness."
               , 'bias' => array("clover"=>"weak-to"
                                ,"candle"=>"neutral"
                                ,"puddle"=>"neutral"
                                ,"spark"=>"resistant"
                                ,"thinkin&rsquo;"=>"neutral"
                                )
               , 'disclaimer' => "checked"
               );
require_once('okaymon-handle.php');
?>
